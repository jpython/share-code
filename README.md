# Share Code

Application Web de partage anonyme de code source.

Projet Share Code : un site à la codeshare.io

## Spécifications informelles :

**L'application Web doit être fonctionnelle même si JavaScript est désactivé sur le navigateur de l'utilisateur** (et ouais...)

On arrive sur une page d’accueil sur
`http://site.com/`

- Titre, message de bienvenue
- Bouton (ou lien) _« Create Snippet »_ qui envoie sur une url :
  `http://site.com/edit/zFy5era6ba` aléatoire où on a pioché dans 
   A-Z/a-z/0-9 (sauf 0, 1, I, l)
- zone textarea de saisie et deux boutons :
  * Save / Save & View
  * Une fois que l'on a cliqué sur _Save_ on retrouve la même page
    et un autre visiteur peut voir/modifier le code sur la même url
  * _Save & View_ envoie sur une url :
    `http://site.com/view/zFy5era6ba` ou on voit le code
    (pas de modification possible)

## Fonctionnalités à ajouter si vous avez le temps

- Voir les premières lignes des dix contributions les plus récentes
  en pas de la page d'accueil du site avec un lien pour les atteindre
- Avoir une page d'admin où l'on peut :
  - Supprimer une contribution
  - Voir la liste des connexions par IP unique et blacklister une 
    adresse IP

## Choix techniques

- Framework Web ou pas ? Flask ou Django ou ... ?
- Ajax ou pas
- Calls API (JSON) en JS ou pas
- Stockage du code
  - Répertoires/fichiers
  - Base de données relationnelle
  - Base de données non-relationnelle, laquelle ?

## Déploiement

- Conteneurs ?
- Orchestrateur Kubernetes ?
- Cloud ou dédié ?
- Serveur HTTP ? Passerelle (UWA)SGI ?
- Colorisation syntaxique avec choix du langage
