Une idée de projet à faire ensemble (quand on aura vu les i/o, les bases de données et les cadriciels Web, jeudi) : une sorte de codeshare.io (allez voir pour avoir une idée):
- partage anonyme d’extrait de code
- bouton nouveau
 -> nouvelle page http://mysite.local/34ryvWaaz
 → formulaire text area pour copier du code
  →  enregistre le code en base lié à l’url

Programming in Python 3 (Second Edition)
A Complete Introduction to the Python Language 
by Mark Summerfield
Python in Practice
Create Better Programs Using Concurrency, Libraries, and Patterns 
by Mark Summerfield

Tuto sur l’analyse d’image en Python 
https://pythonprogramming.net/image-recognition-python/ 




Les modules à la mode :
numpy, pandas, matplotlib, scikit learn, tensorflow, etc.
Livre + code :
https://jakevdp.github.io/PythonDataScienceHandbook/ 

- finir share code (sql/admin/js-html-css)
- déploiement dans AWS de share code (mise en production d’appli Web python)
* comment va se comporter l’appli si le nombre d’entrées du répertoire devient énorme ?
    →  Pour obtenir les 10 plus récentes entrées je suis obligé de mettre dans une liste (en mémoire) toutes les entrées du répertoire data/
    → il faut stocker les métadonnées de façon efficaces : comment ?

* les méta données (date, ip du client, langage du code envoyé) on pourrait les mettre où ?

Andrew Tanenbaum : systèmes d’exploitation

Dans le Shell UNIX quel est le concept proche des générateurs (i.e. du protocole d’itération) de Python ? C’est LE TUBE : |
producteur | consommateur
consommateur(producteur) : Python
~ producteur | consommateur : Shell UNIX
Le blog de Bortz : fondateur du Net en France
https://www.bortzmeyer.org/ 


- des idées de « micro sujets » ?
* Pour tester si une chaîne est un palindrome on a utilisé un test de la forme text[::-1] == text
* Combien de comparaisons sont faites sur les caractères de la chaîne si elle est de longueur n ?
* Peut-on faire mieux ? C’est-à-dire ?
* Implémentez ce « mieux » en Python
    - indices : la fonction all (help(all)) et l’opérateur unaire ~ ( ~ 10 = ?? dunder method __invert__)


- écriture de modules (dernière section du cours)
Projet Share Code : un site à la codeshare.io

https://framagit.org/jpython/share-code 


Un environnement d’exploration de données en Python, complet (python + numpy + pandas + matplotlib + … + Jupyter) : 
anaconda

Modules en C/C++/... + profiling (ex sur framagit)
Q&A, retour sur le contenu de la semaine
Framagit : 
10h50 →  QCM : ordinateur fermé.
- 20 questions
- 1 ou 2 questions où 2 bonnes réponses sont possibles, les autres 1 seule bonne réponse
pour chaque question :
	- 1 bonne réponse          = +1
	- 2 bonnes réponses       = +2
	- 1 bonne + 1 mauvaise = 0

Après midi : déploiement Web (WSGI et cie, conteneur, cloud, etc.)
Monty Python

https://framagit.org/jpython/sieve_profile 

Quelqu’un a essayé d’améliorer le html et d’ajouter du css voir du js à Share Code ?
→ si oui, vous pouvez me l’envoyer ( jp@xiasma.fr )
https://framagit.org/jpython/share-code 
css à mettre dans static/
les gabarits html sont dans templates/

$ git clone https://framagit.org/jpython/share-code 
$ cd share-code
$ python3 -m venv venv
$ source venv/bin/activate (Shell Bourne, sous Windows : git bash, et le activate devrait marcher...)
ou C:\...> .\\venv\Scripts\activate.cmd (ou .bat ou PowerShell, voir votre config...)
(venv) … $ pip install flask 
(venv) … $ mkdir data
(venv) … $ python sharecode.py
http://127.0.0.1:5000/ avec un navigateur
Déploiement d’applications Web Python en production : déployons Share Code sur AWS EC2

Client ← →  réseau ← → Serveur Web ← → Python

Serveur Web : Apache, NGINX, …

« CGI » : lance un processus Python à chaque requète : pas performant…
On utilise WSGI (uWSGI,AWSGI) avec généralement Apache ou NGINX ou gunicorn

Machine physique, Machine virtuelle, VM dans le Cloud, Conteneurs (Docker, rkt, …) avec ou pas un orchestrateur (Kubernetes)

* Je vais le faire dans le Clous AWS EC2
* Je vais le faire dans une VM directement

* Autre possibilité intéressante : Kubernetes et installer N instance Apache+WSGI+Flask+Mon appli dans N conteneurs avec un volume partagé ou alors une base de données (MariaDB, PostgreSQL, noSQL)
+ proxy inverse (natif K8s ou bien NGINX dans un conteneur dédié exposé à l’extérieur)

Bases du cloud AWS EC2 (concepts similaires chez Google Compute Engine, MS Azure, OpenStack, DS Outscale, …)
- VPC ~ switch + cable
- Réseaux (plan d’adressage)
- Passerelles (entre réseaux et vers l’exterieur)
- Machines (virtuelles)
On peut faire ça
- la console Web d’AWS (clic, clic)
- aws-cli
- Python + boto (boto3)
- Terraform : descriptif → j’ai les fichiers près à l’emploi



Mon infra :


Internet → (22,80,443) → bastion (10.0.0.42)
                       → serveur Web (10.0.0.50)

Installer NGINX + uWSGI sur 10.0.0.50
→ suivant un tuto assez simple
nginx → 1 fichier de conf
uWSGI → 1 fichier de conf
(2 services à lancer)
 l’appli est déployée dans /var/www/html/sharecode
avec un point d’entrée wsgi.py
https://www.gab.lc/articles/flask-nginx-uwsgi/
(+répertoire conf du dépot)
En mode dev : on peut tunneler le trafic http vers localhost vers le bastion qui relaie ensuite sur 10.0.0.50.
En mode prod : il faut acheter une elastic-ip (AWS) et l’associer dans le DNS à un nom sharecode.zapto.org et ça marcherait (service gratuit no-ip.com) ou bien acheter un nom de domaine.
