# TODO List

## Add proper HTML5/CSS layout

- Including dark and light themes

## Add a bit of JavaScript

- Application should continue to work with JavaScript disabled
- Add a button near the direct link form entries to copy link to clipboard
- Add a notice when a file has been modified in text area but not saved on server

## Features 

- Syntax highlighting (Pygment)
- Language choice and auto-detection (Pygment)
- Remove a file
- Duplicate a file
- Add JSON api access

## Security

- Optional authentication
- Limit file size
- Detect and refuse binary raw data

## Save file metadata

- Date of modification
- Date of creation (?)
- IP of last modification 
- Owner

## Provide more storage backend options

- SQLLite3
- MariaDB/MySQL
- PostgreSQL
- MongoDB
- ElasticSearch
- S3 

